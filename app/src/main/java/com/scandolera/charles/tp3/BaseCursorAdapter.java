package com.scandolera.charles.tp3;
import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;

public abstract class BaseCursorAdapter<V extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<V> {
    private Cursor Cursor;
    private boolean Valid;
    private int RowIDColumn;
    private Context context;

    public BaseCursorAdapter(Cursor c, Context context) {
        this.context = context;
        setHasStableIds(true);
        swapCursor(c);
    }

    public abstract void onBindViewHolder(V holder, Cursor cursor);

    @Override
    public void onBindViewHolder(V holder, int position) {

        if (!Valid) {
            throw new IllegalStateException("Cannot bind view holder when cursor is in invalid state.");
        }
        if (!Cursor.moveToPosition(position)) {
            throw new IllegalStateException("Could not move cursor to position " + position + " when trying to bind view holder");
        }

        onBindViewHolder(holder, Cursor);
    }

    @Override
    public int getItemCount() {
        if (Valid) {
            return Cursor.getCount();
        } else {
            return 0;
        }
    }

    @Override
    public long getItemId(int position) {
        if (!Valid) {
            throw new IllegalStateException("Cannot lookup item id when cursor is in invalid state.");
        }
        if (!Cursor.moveToPosition(position)) {
            throw new IllegalStateException("Could not move cursor to position " + position + " when trying to get an item id");
        }

        return Cursor.getLong(RowIDColumn);
    }

    public Cursor getItem(int position) {
        if (!Valid) {
            throw new IllegalStateException("Cannot lookup item id when cursor is in invalid state.");
        }
        if (!Cursor.moveToPosition(position)) {
            throw new IllegalStateException("Could not move cursor to position " + position + " when trying to get an item id");
        }
        return Cursor;
    }

    public void swapCursor(Cursor newCursor) {
        if (newCursor == Cursor) {
            return;
        }

        if (newCursor != null) {
            Cursor = newCursor;
            Valid = true;
            // notify the observers about the new cursor
            notifyDataSetChanged();
        } else {
            notifyItemRangeRemoved(0, getItemCount());
            Cursor = null;
            RowIDColumn = -1;
            Valid = false;
        }
    }
}
