package com.scandolera.charles.tp3;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;



public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    public static final String SELECTED_CITY = "SELECTED_CITY";
    private static final int NEW_CITY_ACTIVITY = 1;
    private static final int CITY_ACTIVITY = 2;
    private ListView listView;
    private SwipeRefreshLayout RefreshLayout;
    private WeatherDbHelper dbHelper;
    private SimpleCursorAdapter cursorAdapter;
    private Cursor cursor;

    public static boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, NewCityActivity.class);
                startActivityForResult(intent, NEW_CITY_ACTIVITY);


            }
        });


        RefreshLayout = findViewById(R.id.refresh_layout);
        RefreshLayout.setOnRefreshListener(this);
        listView = findViewById(R.id.listView);
        dbHelper = new WeatherDbHelper(this);
        dbHelper.populate();
        cursor = dbHelper.fetchAllCities();


        cursorAdapter = new SimpleCursorAdapter(this,
                android.R.layout.simple_list_item_2,
                cursor,
                new String[]{WeatherDbHelper.COLUMN_CITY_NAME, WeatherDbHelper.
                        COLUMN_COUNTRY},
                new int[]{android.R.id.text1, android.R.id.text2}, 0);
        listView.setAdapter(cursorAdapter);




        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                cursor.moveToPosition(position);
                City city = dbHelper.cursorToCity(cursor);
                Intent intent = new Intent(MainActivity.this, CityActivity.class);
                intent.putExtra(SELECTED_CITY, city);
                startActivityForResult(intent, CITY_ACTIVITY);

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (CITY_ACTIVITY == requestCode && RESULT_OK == resultCode) {
            City updatedCity = data.getParcelableExtra(MainActivity.SELECTED_CITY);
            dbHelper.updateCity(updatedCity);
            cursor = dbHelper.fetchAllCities();
            cursorAdapter.changeCursor(cursor);
            cursorAdapter.notifyDataSetChanged();
            System.out.println("City :" + updatedCity.getName() + " Country :" + updatedCity.getCountry());
        }
        if (NEW_CITY_ACTIVITY == requestCode && RESULT_OK == resultCode) {
            City newCity = data.getParcelableExtra(NewCityActivity.BUNDLE_EXTRA_NEW_CITY);
            dbHelper.addCity(newCity);
            cursor = dbHelper.fetchAllCities();
            cursorAdapter.changeCursor(cursor);
            cursorAdapter.notifyDataSetChanged();
            System.out.println("City :" + newCity.getName() + " Country :" + newCity.getCountry());

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRefresh() {
        if (MainActivity.isOnline(this))
            new UpdateCitiesWheather().execute();
        else {
            Snackbar.make(findViewById(R.id.refresh_layout), "Vérifiez votre connexion internet", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            RefreshLayout.setRefreshing(false);
        }

    }

    class UpdateCitiesWheather extends AsyncTask<Object, Integer, List<City>> {
        private List<City> Liste;

        @Override
        protected void onPreExecute() {
            Liste = dbHelper.getAllCities();
            super.onPreExecute();
        }

        @Override
        protected List<City> doInBackground(Object... objects) {
            for (City city :Liste) {
                weatherRequest(city);
                dbHelper.updateCity(city);
            }
            return Liste;
        }


        @Override
        protected void onPostExecute(List<City> cities) {
            cursor = dbHelper.fetchAllCities();
            cursorAdapter.changeCursor(cursor);
            cursorAdapter.notifyDataSetChanged();
            RefreshLayout.setRefreshing(false);
            super.onPostExecute(cities);

        }

        private void weatherRequest(City city) {
            HttpURLConnection urlConnection = null;
            try {
                // prepare url
                URL urlToRequest = WebServiceUrl.build(city.getName(), city.getCountry());
                // send a GET request to the serve
                urlConnection = (HttpURLConnection) urlToRequest.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();
                // read data
                InputStream inputStream = urlConnection.getInputStream();
                JSONResponseHandler jsonHandler = new JSONResponseHandler(city);
                jsonHandler.readJsonStream(inputStream);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                urlConnection.disconnect();
            }
        }
    }
}
